package transformation;

import java.util.List;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import entity.IoTSensorsData;
import entity.Statistic;
import it.sabd.uniroma2.miniproject.flink.Query1;
import utils.SensorPlayerIdList;

/**
 * Classe che contiene le trasformazioni che vengono adottate all'interno della query 1
 * @author falberto
 *
 *
 */
public class TransformationQuery1 {

	private static final Logger logger = LoggerFactory.getLogger(Query1.class);
	public static  class IoTSensorDataPlayerFilter implements FilterFunction<IoTSensorsData> {
	      
		private static final long serialVersionUID = 1L;

	@Override
      public boolean filter(IoTSensorsData event) {
        return  4 != Long.parseLong(event.getSid()) &&
        		8 != Long.parseLong(event.getSid()) &&
        	    10 != Long.parseLong(event.getSid()) &&
        		12 != Long.parseLong(event.getSid()) &&
        		105 != Long.parseLong(event.getSid()) &&
        		106 != Long.parseLong(event.getSid()) &&
        		97 != Long.parseLong(event.getSid()) &&
        		98 != Long.parseLong(event.getSid()) &&
        		99 != Long.parseLong(event.getSid()) &&
        		100 != Long.parseLong(event.getSid()) ;
        		
      }
    }
	
	public static class IoTSensorDataKeySelector implements KeySelector<IoTSensorsData, String>{
		
		private static final long serialVersionUID = 1L;
		@Override
        public String getKey(IoTSensorsData event) throws Exception {
            return event.getSid();
        }
	} 
	
	public static class IoTSensorDataWindowFunction implements WindowFunction<IoTSensorsData, Tuple2<String,Statistic>, 
	String, TimeWindow> {
		
		private static final long serialVersionUID = 1L;

		@Override
        public void apply(String id, TimeWindow window, Iterable<IoTSensorsData> values, 
        		Collector<Tuple2<String,Statistic>> out) {
            int count = 0;
            long agg = 0;
            double precX = 0.0;
            double precY = 0.0;
            double totDist = 0.0;
            for (IoTSensorsData r : values) {
                agg += r.getModV();
                count++;
                if(count ==1){
                   precX = r.getX();
                   precY = r.getY();
                }
                totDist +=  Math.sqrt((r.getX() - precX) * (r.getX() - precX) + 
                (r.getY() - precY) * (r.getY() - precY));
                precX = r.getX();
                precY = r.getY();
            }
            out.collect(new Tuple2<String,Statistic>
            (id, new Statistic(window.getStart(),  window.getEnd(),
            		totDist, agg / count )));
        }
	}
	
	public static class IoTSensorDataToPlayerMapper implements MapFunction<Tuple2<String,Statistic>, Statistic> {
        private static final long serialVersionUID = -6867736771747690202L;

        @Override
        public Statistic map(Tuple2<String,Statistic> value) 
        		throws Exception {
            List<Tuple2<String,String>> sensorsPlayerId=  
            		SensorPlayerIdList.getInstance();
            for(Tuple2<String,String> s: sensorsPlayerId){
            	if(s.f0.equals(value.f0)){
            		logger.info("MAP!" + new Statistic(value.f1.getTsStart(),
            				value.f1.getTsStop(),
            				s.f1,
            				value.f1.getTotalDistance(),
            				value.f1.getAverageSpeed()).toEmitOutput());
            		return new Statistic(value.f1.getTsStart(),
            				value.f1.getTsStop(),
            				s.f1,
            				value.f1.getTotalDistance(),
            				value.f1.getAverageSpeed());
            	}
            	 
            }
            return new Statistic(value.f1.getTsStart(),
    				value.f1.getTsStop(),
    				"Uknown Player!",
    				value.f1.getTotalDistance(),
    				value.f1.getAverageSpeed());
        }
      
	
	}
	
	public static class IoTSensorDataPlayerKeySelector implements KeySelector<Statistic, String> {      
		private static final long serialVersionUID = 1L;
		@Override
        public String getKey(Statistic event) throws Exception {
			logger.info("KeyByID!\n" +event.toEmitOutput()+"\n");
			return event.getPlayerId();
        }
    }
	
	public static class IoTSensorDataSensorsReducer implements ReduceFunction<Statistic> {
    	static final long serialVersionUID = 1L;
		@Override
        public Statistic reduce(
        		Statistic value1, Statistic value2)
        throws Exception {
			//fare qui la media delle medie
			Statistic stat = new Statistic(
					//value2.getTsStart(),
					value2.getTsStart(),
					value2.getTsStop(),
					value2.getPlayerId(),
					(value1.getTotalDistance() +
					value2.getTotalDistance())/2,
					(value1.getAverageSpeed() +
					 value2.getAverageSpeed())/2
					);
			logger.info("RIDUCO!\n" + "1)" + value1.toEmitOutput() + "\n2)"
					+value2.toEmitOutput()+"\n");
            return stat;
        }
    }
	
	public static class IoTSensorDataSensorsOutputMapper implements MapFunction<Statistic, String> {
        private static final long serialVersionUID = -6867736771747690202L;

        @Override
        public String map(Statistic value) 
        		throws Exception {
          
            return value.toEmitOutput();
        }
      }
}
