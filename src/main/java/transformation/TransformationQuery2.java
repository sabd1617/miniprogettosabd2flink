package transformation;

import java.util.List;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import entity.IoTSensorsData;
import entity.Statistic;
import entity.TopFive;
import utils.IterableToList;
import utils.SensorPlayerIdList;
/**
 * Classe che contiene le trasformazioni che vengono utilizzate all'interno della query2
 * 
 * @author falberto
 *
 */
public class TransformationQuery2 {

	public static class IoTSensorDataPlayerFilter implements FilterFunction<IoTSensorsData> {
	      
		private static final long serialVersionUID = 1L;

	@Override
      public boolean filter(IoTSensorsData event) {
        return  4 != Long.parseLong(event.getSid()) &&
        		8 != Long.parseLong(event.getSid()) &&
        	    10 != Long.parseLong(event.getSid()) &&
        		12 != Long.parseLong(event.getSid()) &&
        		105 != Long.parseLong(event.getSid()) &&
        		106 != Long.parseLong(event.getSid()) &&
        		97 != Long.parseLong(event.getSid()) &&
        		98 != Long.parseLong(event.getSid()) &&
        		99 != Long.parseLong(event.getSid()) &&
        		100 != Long.parseLong(event.getSid()) ;
        		
      }
    }
	
	public static class IoTSensorDataKeySelector implements KeySelector<IoTSensorsData, String>{
		
		private static final long serialVersionUID = 1L;
		@Override
        public String getKey(IoTSensorsData event) throws Exception {
            return event.getSid();
        }
	} 
	
	public static class IoTSensorDataWindowFunction implements WindowFunction<IoTSensorsData, Tuple2<String,Statistic>, 
		String, TimeWindow> {

			private static final long serialVersionUID = 1L;
			
			@Override
			public void apply(String id, TimeWindow window, Iterable<IoTSensorsData> values, 
					Collector<Tuple2<String,Statistic>> out) {
			    int count = 0;
			    long agg = 0;
			    for (IoTSensorsData r : values) {
			        agg += r.getModV();
			        count++;           
			    }
			    out.collect(new Tuple2<String,Statistic>
			    (id, new Statistic(window.getStart(),  window.getEnd(), 
			    		agg / count )));
			}
	}
	
	public static class IoTSensorDataToPlayerMapper implements 
			MapFunction<Tuple2<String,Statistic>, Statistic> {
        private static final long serialVersionUID = -6867736771747690202L;

        @Override
        public Statistic map(Tuple2<String,Statistic> value) 
        		throws Exception {
            List<Tuple2<String,String>> sensorsPlayerId=  
            		SensorPlayerIdList.getInstance();
            for(Tuple2<String,String> s: sensorsPlayerId){
            	if(s.f0.equals(value.f0)){
            		return new Statistic(value.f1.getTsStart(),
            				value.f1.getTsStop(),
            				s.f1,
            				value.f1.getAverageSpeed());
            	}
            	 
            }
            return new Statistic(value.f1.getTsStart(),
    				value.f1.getTsStop(),
    				"Uknown Player!",
    				value.f1.getAverageSpeed());
        }
      }
	
	public static class IoTSensorDataPlayerKeySelector implements 
			KeySelector<Statistic, String> {      
		private static final long serialVersionUID = 1L;
		@Override
        public String getKey(Statistic event) throws Exception {
            return event.getPlayerId();
        }
    }
	
	public static class IoTSensorDataSensorsReducer implements 
		ReduceFunction<Statistic>{
    	static final long serialVersionUID = 1L;
		@Override
        public Statistic reduce(
        		Statistic value1, Statistic value2)
        throws Exception {
			//fare qui la media delle medie
			Statistic stat = new Statistic(
					value2.getTsStart(),
					value2.getTsStop(),
					value2.getPlayerId(),
					(value1.getAverageSpeed() +
					 value2.getAverageSpeed())/2
					);
					
            return stat;
        }
    }
	
	public static class IoTSensorDataTtartKeySelector implements 
			KeySelector<Statistic, Long> {      
 			private static final long serialVersionUID = 1L;
 			@Override
             public Long getKey(Statistic event) throws Exception {
                 return event.getTsStart();
             }
         }
	
	public static class IoTSensorDataClassificWindow implements 
		WindowFunction<Statistic, String, Long, GlobalWindow>{
		
			private static final long serialVersionUID = 1L;
			@Override
			public void apply (Long key,
					GlobalWindow window,
		            Iterable<Statistic> values,
		            Collector<String> out) throws Exception {
		        List<Statistic> statisticList = 
		        		IterableToList.toList((values));
		        IterableToList.bubbleSort(statisticList);
		       
		        TopFive topFive = new TopFive();
		        topFive.setTsStart(statisticList.get(statisticList.size()-1).getTsStart());
		        topFive.setTsStop(statisticList.get(statisticList.size()-1).getTsStop());
		        topFive.setPlayerId1(statisticList.get(statisticList.size()-1).getPlayerId());
		        topFive.setAvgSpeed1(statisticList.get(statisticList.size()-1).getAverageSpeed());
		        topFive.setPlayerId2(statisticList.get(statisticList.size()-2).getPlayerId());
		        topFive.setAvgSpeed2(statisticList.get(statisticList.size()-2).getAverageSpeed());       		        
		        topFive.setPlayerId3(statisticList.get(statisticList.size()-3).getPlayerId());
		        topFive.setAvgSpeed3(statisticList.get(statisticList.size()-3).getAverageSpeed());
		        topFive.setPlayerId4(statisticList.get(statisticList.size()-4).getPlayerId());
		        topFive.setAvgSpeed4(statisticList.get(statisticList.size()-4).getAverageSpeed());
		        topFive.setPlayerId5(statisticList.get(statisticList.size()-5).getPlayerId());
		        topFive.setAvgSpeed5(statisticList.get(statisticList.size()-5).getAverageSpeed());
		       
		        out.collect (topFive.toEmitOutput());
			}
		}
}
