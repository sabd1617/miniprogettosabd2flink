package entity;

import java.io.Serializable;

public class IoTSensorsDataFiltered implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String sid;
	private long ts;
	private long x;
	private long y;
	
	private long modV;

	
	public IoTSensorsDataFiltered() {
		super();
	}

	public IoTSensorsDataFiltered(String sid, long ts, long x, long y, long modV) {
		super();
		this.sid = sid;
		this.ts = ts;
		this.x = x;
		this.y = y;
		this.modV = modV;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	public long getX() {
		return x;
	}

	public void setX(long x) {
		this.x = x;
	}

	public long getY() {
		return y;
	}

	public void setY(long y) {
		this.y = y;
	}

	public long getModV() {
		return modV;
	}

	public void setModV(long modV) {
		this.modV = modV;
	}

	@Override
	public String toString() {
		return "IoTSensorsDataFiltered [sid=" + sid + ", ts=" + ts + ", x=" + x + ", y=" + y + ", modV=" + modV + "]";
	}
	
	
}
