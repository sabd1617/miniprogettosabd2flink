package entity;

import java.io.Serializable;

// sid, ts, x, y, z, |v|, |a|, vx, vy, vz, ax, ay, az
public class IoTSensorsData implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String sid;
	private long ts;
	private long x;
	private long y;
	private long z;
	private long modV;
	private long modA;
	private long vx;
	private long vy;
	private long vz;
	private long ax;
	private long ay;
	private long az;
	
	
	public IoTSensorsData() {
		super();
	}
	public IoTSensorsData(long ts, long x, long y, long z, long modV, long modA, long vx, long vy, long vz, long ax,
			long ay, long az) {
		super();
		this.ts = ts;
		this.x = x;
		this.y = y;
		this.z = z;
		this.modV = modV;
		this.modA = modA;
		this.vx = vx;
		this.vy = vy;
		this.vz = vz;
		this.ax = ax;
		this.ay = ay;
		this.az = az;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public long getTs() {
		return ts;
	}
	public void setTs(long ts) {
		this.ts = ts;
	}
	public long getX() {
		return x;
	}
	public void setX(long x) {
		this.x = x;
	}
	public long getY() {
		return y;
	}
	public void setY(long y) {
		this.y = y;
	}
	public long getZ() {
		return z;
	}
	public void setZ(long z) {
		this.z = z;
	}
	public long getModV() {
		return modV;
	}
	public void setModV(long modV) {
		this.modV = modV;
	}
	public long getModA() {
		return modA;
	}
	public void setModA(long modA) {
		this.modA = modA;
	}
	public long getVx() {
		return vx;
	}
	public void setVx(long vx) {
		this.vx = vx;
	}
	public long getVy() {
		return vy;
	}
	public void setVy(long vy) {
		this.vy = vy;
	}
	public long getVz() {
		return vz;
	}
	public void setVz(long vz) {
		this.vz = vz;
	}
	public long getAx() {
		return ax;
	}
	public void setAx(long ax) {
		this.ax = ax;
	}
	public long getAy() {
		return ay;
	}
	public void setAy(long ay) {
		this.ay = ay;
	}
	public long getAz() {
		return az;
	}
	public void setAz(long az) {
		this.az = az;
	}
	
}