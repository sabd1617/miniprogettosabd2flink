package entity;

public class TopFive {

	/** The timestamp of the aggregate */
    private long tsStart;
    
    private long tsStop;
    
    private String playerId1;
    
    /** The aggregate value */
    private double avgSpeed1;
    
    private String playerId2;
    
    /** The aggregate value */
    private double avgSpeed2;
    
    private String playerId3;
    
    /** The aggregate value */
    private double avgSpeed3;
    
    private String playerId4;
    
    /** The aggregate value */
    private double avgSpeed4;
    
    private String playerId5;
    
    /** The aggregate value */
    private double avgSpeed5;

    
	public TopFive() {
		super();
	}

	public TopFive(long tsStart, long tsStop, String playerId1, double avgSpeed1, String playerId2, double avgSpeed2,
			String playerId3, double avgSpeed3, String playerId4, double avgSpeed4, String playerId5,
			double avgSpeed5) {
		super();
		this.tsStart = tsStart;
		this.tsStop = tsStop;
		this.playerId1 = playerId1;
		this.avgSpeed1 = avgSpeed1;
		this.playerId2 = playerId2;
		this.avgSpeed2 = avgSpeed2;
		this.playerId3 = playerId3;
		this.avgSpeed3 = avgSpeed3;
		this.playerId4 = playerId4;
		this.avgSpeed4 = avgSpeed4;
		this.playerId5 = playerId5;
		this.avgSpeed5 = avgSpeed5;
	}

	public long getTsStart() {
		return tsStart;
	}

	public void setTsStart(long tsStart) {
		this.tsStart = tsStart;
	}

	public long getTsStop() {
		return tsStop;
	}

	public void setTsStop(long tsStop) {
		this.tsStop = tsStop;
	}

	public String getPlayerId1() {
		return playerId1;
	}

	public void setPlayerId1(String playerId1) {
		this.playerId1 = playerId1;
	}

	public double getAvgSpeed1() {
		return avgSpeed1;
	}

	public void setAvgSpeed1(double avgSpeed1) {
		this.avgSpeed1 = avgSpeed1;
	}

	public String getPlayerId2() {
		return playerId2;
	}

	public void setPlayerId2(String playerId2) {
		this.playerId2 = playerId2;
	}

	public double getAvgSpeed2() {
		return avgSpeed2;
	}

	public void setAvgSpeed2(double avgSpeed2) {
		this.avgSpeed2 = avgSpeed2;
	}

	public String getPlayerId3() {
		return playerId3;
	}

	public void setPlayerId3(String playerId3) {
		this.playerId3 = playerId3;
	}

	public double getAvgSpeed3() {
		return avgSpeed3;
	}

	public void setAvgSpeed3(double avgSpeed3) {
		this.avgSpeed3 = avgSpeed3;
	}

	public String getPlayerId4() {
		return playerId4;
	}

	public void setPlayerId4(String playerId4) {
		this.playerId4 = playerId4;
	}

	public double getAvgSpeed4() {
		return avgSpeed4;
	}

	public void setAvgSpeed4(double avgSpeed4) {
		this.avgSpeed4 = avgSpeed4;
	}

	public String getPlayerId5() {
		return playerId5;
	}

	public void setPlayerId5(String playerId5) {
		this.playerId5 = playerId5;
	}

	public double getAvgSpeed5() {
		return avgSpeed5;
	}

	public void setAvgSpeed5(double avgSpeed5) {
		this.avgSpeed5 = avgSpeed5;
	}

	
	public String  toEmitOutput() {
		return Long.toString(tsStart) + "," +
				Long.toString(tsStop) + "," + 
			   playerId1 +"," + 
			   Double.toString(avgSpeed1) +"," +
			   playerId2 +"," +
			   Double.toString(avgSpeed2) +"," + 
			   playerId3 +"," +
			   Double.toString(avgSpeed3) +"," + 
			   playerId4 +"," + 
			   Double.toString(avgSpeed4) +"," +
			   playerId5 +"," + 
			   Double.toString(avgSpeed5);
	}
    
	@Override
	public String toString() {
		return "TopFive [tsStart=" + tsStart + ", tsStop=" + tsStop + ", playerId1=" + playerId1 + ", avgSpeed1="
				+ avgSpeed1 + ", playerId2=" + playerId2 + ", avgSpeed2=" + avgSpeed2 + ", playerId3=" + playerId3
				+ ", avgSpeed3=" + avgSpeed3 + ", playerId4=" + playerId4 + ", avgSpeed4=" + avgSpeed4 + ", playerId5="
				+ playerId5 + ", avgSpeed5=" + avgSpeed5 + "]";
	}
}
