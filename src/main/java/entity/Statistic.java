package entity;

public class Statistic {
    
    /** The timestamp of the aggregate */
    private long tsStart;
    
    private long tsStop;
    
    private String playerId;
    
    /** The aggregate value */
    private double totalDistance;
    
    /** The aggregate value */
    private double averageSpeed;
    
    

	public Statistic(long tsStart, long tsStop, String playerId, double averageSpeed) {
		super();
		this.tsStart = tsStart;
		this.tsStop = tsStop;
		this.playerId = playerId;
		this.averageSpeed = averageSpeed;
	}

	public Statistic(long tsStart, long tsStop, double averageSpeed) {
		super();
		this.tsStart = tsStart;
		this.tsStop = tsStop;
		this.averageSpeed = averageSpeed;
	}

	public Statistic() {
		super();
	}

	public Statistic(long tsStart, long tsStop, double totalDistance, double averageSpeed) {
		super();
		this.tsStart = tsStart;
		this.tsStop = tsStop;
		this.totalDistance = totalDistance;
		this.averageSpeed = averageSpeed;
	}


	public Statistic(long tsStart, long tsStop, String playerId, double totalDistance, double averageSpeed) {
		super();
		this.tsStart = tsStart;
		this.tsStop = tsStop;
		this.playerId = playerId;
		this.totalDistance = totalDistance;
		this.averageSpeed = averageSpeed;
	}

	public long getTsStart() {
		return tsStart;
	}

	public void setTsStart(long tsStart) {
		this.tsStart = tsStart;
	}

	public long getTsStop() {
		return tsStop;
	}

	public void setTsStop(long tsStop) {
		this.tsStop = tsStop;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public double getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}

	public double getAverageSpeed() {
		return averageSpeed;
	}

	public void setAverageSpeed(double averageSpeed) {
		this.averageSpeed = averageSpeed;
	}

	@Override
	public String toString() {
		return "Statistic [tsStart=" + tsStart + ", tsStop=" + tsStop + ", playerId=" + playerId + ", totalDistance="
				+ totalDistance + ", averageSpeed=" + averageSpeed + "]";
	}
    
	public String toEmitOutput() {
		return Long.toString(tsStart) + ","+ Long.toString(tsStop)+
				","+ playerId + "," + Double.toString(totalDistance) +","+
				 Double.toString(averageSpeed);
	}
    
    
}