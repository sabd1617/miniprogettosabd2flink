package conf;

public class ConfQuery1 {

	public static final String nameTopic = "sabd";
	
	public static final String bootstrapServersProperty = "localhost:9092";
	public static final String groupIdProperty = "flink-consumer";
	public static final String zookeeperConnectProperty = "localhost:2181";
	public static final String autoOffsetResetProperty = "earliest";
	
	public static final long startMatch = 10629342490369879L;
	
	public static final String outputOneMinute = "/home/falberto/Scrivania/out/query1/1";
	public static final String outputFiveMinute = "/home/falberto/Scrivania/out/query1/5";
	public static final String outputAllMatch = "/home/falberto/Scrivania/out/query1/all";
	
	public static final String jobName = "Query 1";
	
}
