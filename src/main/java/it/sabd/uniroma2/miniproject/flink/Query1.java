package it.sabd.uniroma2.miniproject.flink;

import java.util.Properties;

import org.apache.flink.core.fs.FileSystem.WriteMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;


import org.slf4j.LoggerFactory;

import conf.ConfQuery1;

import org.slf4j.Logger;

import transformation.TransformationQuery1.IoTSensorDataPlayerFilter;
import transformation.TransformationQuery1.IoTSensorDataKeySelector;
import transformation.TransformationQuery1.IoTSensorDataWindowFunction;
import transformation.TransformationQuery1.IoTSensorDataToPlayerMapper;
import transformation.TransformationQuery1.IoTSensorDataPlayerKeySelector;
import transformation.TransformationQuery1.IoTSensorDataSensorsReducer;
import transformation.TransformationQuery1.IoTSensorDataSensorsOutputMapper;

import entity.IoTSensorsData;
import utils.IoTDataSensorsSchema;
import utils.ThroughputLogger;

/**
 * Questa classe contiene la definizione della query1
 * proposta durante il corso di SABD per l'anno accademico 2016/2017 come primo quesito
 * del secondo miniprogetto di fine corso.
 * 
 * Si riporta il testo della query1:
 * 
 *  	"1. Analizzare le prestazioni nella corsa di ogni giocatore che partecipa 
 *  	alla partita. L’output della statistica aggregata sulla corsa ha il seguente schema:
 *		
 *			ts_start, ts_stop, player_id, total distance, avg speed 
 * 
 *	    dove
 *	 	ts_start, //start of the run
 *	 	ts_stop, //end of the run
 *	 	player_id, //identifier of a player for which the measurement is made
 *	 	total distance, //total length of the run performed by the player
 *	 	avg speed //average speed of the run
 *	  	
 *	   Tali statistiche aggregate dovranno essere calcolate per diverse finestre temporali, in modo tale da
 *	   permettere di confrontare le prestazioni di ciascun giocatore durante lo svolgimento della partita. Le
 *	   finestre temporali richieste hanno durata di:
 *	 	
 *	 	• 1 minuto;
 *	 	• 5 minuti;
 *	 	• l’intera partita."
 * 
 * @author falberto
 *
 */


public class Query1 {
	
	private static final Logger logger = LoggerFactory.getLogger(Query1.class);

	public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // configure event-time characteristics
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", ConfQuery1.bootstrapServersProperty);
        properties.setProperty("group.id", ConfQuery1.groupIdProperty);
        properties.setProperty("zookeeper.connect", ConfQuery1.zookeeperConnectProperty);
        properties.setProperty("auto.offset.reset", ConfQuery1.autoOffsetResetProperty);  
        
        // Always read topic from start
        DataStream<IoTSensorsData> stream = env.addSource(new FlinkKafkaConsumer09<>(
            ConfQuery1.nameTopic,new IoTDataSensorsSchema(), properties) );
       

        /*// LOG Performance relative al throughput
        stream.flatMap(new ThroughputLogger<IoTSensorsData>(
        		160, 
        		1000000));*/
            
      DataStream<IoTSensorsData> withTimestampsAndWatermarks =
        		stream
        		.filter(new IoTSensorDataPlayerFilter())
        		.assignTimestampsAndWatermarks(
        				new AscendingTimestampExtractor<IoTSensorsData>() {

					private static final long serialVersionUID = 1L;

					@Override
        	        public long extractAscendingTimestamp(IoTSensorsData element) {
						logger.info(Long.toString(element.getTs() 
								- ConfQuery1.startMatch));
        	            return element.getTs() - ConfQuery1.startMatch;
        	        }
        	});
        
      //finestra da un minuto
      withTimestampsAndWatermarks
        .keyBy(new IoTSensorDataKeySelector())
        .timeWindow(Time.seconds(60* 1000000000L))
        .apply(new IoTSensorDataWindowFunction())     
        //faccio la join
        .map(new IoTSensorDataToPlayerMapper())
        .keyBy(new IoTSensorDataPlayerKeySelector())
        .countWindow(2)
        .reduce(new IoTSensorDataSensorsReducer())
        .map(new IoTSensorDataSensorsOutputMapper())
        .writeAsText(ConfQuery1.outputOneMinute, WriteMode.OVERWRITE);
      	
      
      //finestra da 5 minuti
      withTimestampsAndWatermarks
      .keyBy(new IoTSensorDataKeySelector())
      .timeWindow(Time.seconds(300* 1000000000L))
      .apply(new IoTSensorDataWindowFunction())     
      //faccio la join
      .map(new IoTSensorDataToPlayerMapper())
      .keyBy(new IoTSensorDataPlayerKeySelector())
      .countWindow(2)
      .reduce(new IoTSensorDataSensorsReducer())
      .map(new IoTSensorDataSensorsOutputMapper())
      .writeAsText(ConfQuery1.outputFiveMinute, WriteMode.OVERWRITE);
   
      //in questa versione non viene gestita la finestra globale
      
      env.execute(ConfQuery1.jobName);
    }
    
}