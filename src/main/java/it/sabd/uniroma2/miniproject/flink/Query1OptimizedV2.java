package it.sabd.uniroma2.miniproject.flink;

import java.util.Properties;

import org.apache.flink.core.fs.FileSystem.WriteMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import conf.ConfQuery1;
import entity.IoTSensorsDataFiltered;
import entity.Statistic;
import transformation.TransformationQuery1Optimized.IoTSensorDataFilteredPlayerKeySelector;
import transformation.TransformationQuery1Optimized.IoTSensorDataFilteredSensorsOutputMapper;
import transformation.TransformationQuery1Optimized.IoTSensorDataFilteredSensorsReducer;
import transformation.TransformationQuery1Optimized.IoTSensorDataFilteredToPlayerMapper;
import transformation.TransformationQuery1Optimized.IoTSensorDataPlayerFilter;
import transformation.TransformationQuery1Optimized.IoTSensorsDataFilteredKeySelector;
import transformation.TransformationQuery1Optimized.IoTSensorsDataFilteredMapper;
import transformation.TransformationQuery1Optimized.IoTSensorsDataFilteredWindowFunction;
import transformation.TransformationQuery1Optimized.StatisticWindowFunction;

import transformation.TransformationQuery1Optimized.StatisticWindowSessionFunction;


/**
 * Questa classe contiene la definizione della query1
 * proposta durante il corso di SABD per l'anno accademico 2016/2017 come primo quesito
 * del secondo miniprogetto di fine corso. In particolare questa versione è più ottimizzata,
 * perché oltre ad offrire i vantaggi della versione ottimizzata, consente di disporre a valle
 * della topologia la diramazione dello stream principale negli stream relativi alla finestra 
 * globale e alla finestra dei 5 minuti, evitando la trasmissione a monte di una grande mole di dati
 * In questa versione si paga in termini di precisione, perché le stime su questa finestra sono operate
 * rispetto alle stime relative alla finestra di un minuto, aggregando tali valori 
 * piuttosto che ricalcolarli nuovamente.
 * 
 * Si riporta il testo della query1:
 * 
 *  	"1. Analizzare le prestazioni nella corsa di ogni giocatore che partecipa 
 *  	alla partita. L’output della statistica aggregata sulla corsa ha il seguente schema:
 *		
 *			ts_start, ts_stop, player_id, total distance, avg speed 
 * 
 *	    dove
 *	 	ts_start, //start of the run
 *	 	ts_stop, //end of the run
 *	 	player_id, //identifier of a player for which the measurement is made
 *	 	total distance, //total length of the run performed by the player
 *	 	avg speed //average speed of the run
 *	  	
 *	   Tali statistiche aggregate dovranno essere calcolate per diverse finestre temporali, in modo tale da
 *	   permettere di confrontare le prestazioni di ciascun giocatore durante lo svolgimento della partita. Le
 *	   finestre temporali richieste hanno durata di:
 *	 	
 *	 	• 1 minuto;
 *	 	• 5 minuti;
 *	 	• l’intera partita."
 * 
 * @author falberto
 *
 */
public class Query1OptimizedV2 {

	private static final Logger logger = LoggerFactory.getLogger(Query1OptimizedV2.class);
	
	public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // configure event-time characteristics
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", ConfQuery1.bootstrapServersProperty);
        properties.setProperty("group.id", ConfQuery1.groupIdProperty);
        properties.setProperty("zookeeper.connect", ConfQuery1.zookeeperConnectProperty);
        properties.setProperty("auto.offset.reset", ConfQuery1.autoOffsetResetProperty);  
        // Always read topic from start
        DataStream<String> stream = env.addSource(new FlinkKafkaConsumer09<>(
        	ConfQuery1.nameTopic, new SimpleStringSchema(), properties) );
           
      DataStream<IoTSensorsDataFiltered> withTimestampsAndWatermarks =
        		stream
        		.map(new IoTSensorsDataFilteredMapper())
        		//.filter(new IoTSensorDataPlayerFilterOptimizedDx())
        		//.filter(new IoTSensorDataPlayerFilterOptimizedSx())
        		.filter(new IoTSensorDataPlayerFilter())
        		.assignTimestampsAndWatermarks(
        				new AscendingTimestampExtractor<IoTSensorsDataFiltered>() {

					private static final long serialVersionUID = 1L;

					@Override
        	        public long extractAscendingTimestamp(IoTSensorsDataFiltered element) {
						logger.info(Long.toString(element.getTs() 
								- ConfQuery1.startMatch));
        	            return element.getTs() - ConfQuery1.startMatch;
        	        }
        	});
      
        //Finestra su 1 minuto
        DataStream<Statistic> stream2 =  withTimestampsAndWatermarks
        .keyBy(new IoTSensorsDataFilteredKeySelector())
        .timeWindow(Time.seconds(60* 1000000000L))
        .apply( new IoTSensorsDataFilteredWindowFunction())     
        //faccio la join
        .map(new IoTSensorDataFilteredToPlayerMapper())
        //questo blocco è quello relativo al merge dei due sensori (commentare per unico sensore)	       
        .keyBy(new IoTSensorDataFilteredPlayerKeySelector())
        .countWindow(2)
        .reduce(new IoTSensorDataFilteredSensorsReducer());
         
        //Output Finestra un minuto
        stream2.map(new IoTSensorDataFilteredSensorsOutputMapper())
        .writeAsText(ConfQuery1.outputOneMinute, WriteMode.OVERWRITE);
        
        //Finestra su 5 minuti
        stream2.keyBy(new IoTSensorDataFilteredPlayerKeySelector())
        .countWindow(5)
        .apply(new StatisticWindowFunction())
        .map(new IoTSensorDataFilteredSensorsOutputMapper())
        .writeAsText(ConfQuery1.outputFiveMinute, WriteMode.OVERWRITE);
        
        //tutta la partita
        stream2.keyBy(new IoTSensorDataFilteredPlayerKeySelector())
        .window(ProcessingTimeSessionWindows.withGap(Time.seconds(300)))
        .apply( new StatisticWindowSessionFunction()) 
        .map(new IoTSensorDataFilteredSensorsOutputMapper())
        .writeAsText(ConfQuery1.outputAllMatch, WriteMode.OVERWRITE);
             
        env.execute(ConfQuery1.jobName);
    }
}
