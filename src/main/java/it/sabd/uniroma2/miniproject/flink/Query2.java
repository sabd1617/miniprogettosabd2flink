package it.sabd.uniroma2.miniproject.flink;

import java.util.Properties;

import org.apache.flink.core.fs.FileSystem.WriteMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;

import conf.ConfQuery2;
import transformation.TransformationQuery2.IoTSensorDataPlayerFilter;
import transformation.TransformationQuery2.IoTSensorDataKeySelector;
import transformation.TransformationQuery2.IoTSensorDataWindowFunction;
import transformation.TransformationQuery2.IoTSensorDataToPlayerMapper;
import transformation.TransformationQuery2.IoTSensorDataPlayerKeySelector;
import transformation.TransformationQuery2.IoTSensorDataSensorsReducer;
import transformation.TransformationQuery2.IoTSensorDataTtartKeySelector;
import transformation.TransformationQuery2.IoTSensorDataClassificWindow;

import entity.IoTSensorsData;
import utils.IoTDataSensorsSchema;

/**
 * Questa classe contiene la definizione della query2
 * proposta durante il corso di SABD per l'anno accademico 2016/2017 come primo quesito
 * del secondo miniprogetto di fine corso.
 * 
 * Si riporta il testo della query2:
 * 
 *	  " 2. A complemento della query precedente, si richiede di fornire la classifica 
 *		   aggiornata in tempo reale dei 5 giocatori piu veloci. 
 *		   L’output della classifica ha il seguente schema: `
 *		 			
 *		 		ts_start, ts_stop, playerid_1, avg speed1, player_id2, avg speed2,
 *		 		player_id3, avg speed3, player_id4, avg speed4, player_id5,
 *		 		avg_speed5
 *
 *		   dove
 *		   ts_start, //start of the run
 *		   ts_stop, // end of the run
 *		   player_id1, //identifier of the fastest player
 *		   avg speed1 //average speed of the run of the fastest player
 *		   player_id2, //identifier of the second fastest player
 *		   avg speed2 //average speed of the run of the second fastest player
 *		   ...
 *		 	  
 *		 Tali statistiche aggregate dovranno essere calcolate per diverse finestre temporali, in modo tale da
 *		 permettere di confrontare le prestazioni dei giocatori piu veloci durante lo svolgimento della partita. `
 *		 Le finestre temporali richieste hanno durata di:
 *		 	
 *		 	• 1 minuto;
 *		 	• 5 minuti;
 *		 	• l’intera partita."
 *		 
 */

public class Query2 {

	public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // configure event-time characteristics
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", ConfQuery2.bootstrapServersProperty);
        properties.setProperty("group.id", ConfQuery2.groupIdProperty);
        properties.setProperty("zookeeper.connect", ConfQuery2.zookeeperConnectProperty);
        properties.setProperty("auto.offset.reset", ConfQuery2.autoOffsetResetProperty);       // Always read topic from start
        DataStream<IoTSensorsData> stream = env.addSource(new FlinkKafkaConsumer09<>(
            ConfQuery2.nameTopic,new IoTDataSensorsSchema(), properties) );
        
      DataStream<IoTSensorsData> withTimestampsAndWatermarks =
        		stream
        		.filter(new IoTSensorDataPlayerFilter())
        		.assignTimestampsAndWatermarks(
        				new AscendingTimestampExtractor<IoTSensorsData>() {

					private static final long serialVersionUID = 1L;

					@Override
        	        public long extractAscendingTimestamp(IoTSensorsData element) {
        	            return element.getTs() - ConfQuery2.startMatch;
        	        }
        	});
        //Tempo 1 minuto
        withTimestampsAndWatermarks
        .keyBy(new IoTSensorDataKeySelector())
        .timeWindow(Time.seconds(60* 1000000000L))
        .apply(new IoTSensorDataWindowFunction())        
        //faccio la join
        .map(new IoTSensorDataToPlayerMapper())
        .keyBy(new IoTSensorDataPlayerKeySelector())
        .countWindow(2)
        .reduce(new IoTSensorDataSensorsReducer())
        .keyBy(new IoTSensorDataTtartKeySelector())
        .countWindow(16)
        .apply(new IoTSensorDataClassificWindow())
        .writeAsText(ConfQuery2.outputOneMinute, WriteMode.OVERWRITE);
        
        //Tempo 5 minuti
        withTimestampsAndWatermarks
        .keyBy(new IoTSensorDataKeySelector())
        .timeWindow(Time.seconds(300* 1000000000L))
        .apply(new IoTSensorDataWindowFunction())        
        //faccio la join
        .map(new IoTSensorDataToPlayerMapper())
        .keyBy(new IoTSensorDataPlayerKeySelector())
        .countWindow(2)
        .reduce(new IoTSensorDataSensorsReducer())
        .keyBy(new IoTSensorDataTtartKeySelector())
        .countWindow(16)
        .apply(new IoTSensorDataClassificWindow())
        .writeAsText(ConfQuery2.outputFiveMinute, WriteMode.OVERWRITE);
        
      //in questa versione non viene gestita la finestra globale
        
        env.execute(ConfQuery2.jobName);
    }
     
}
