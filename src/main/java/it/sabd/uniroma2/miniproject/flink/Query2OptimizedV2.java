package it.sabd.uniroma2.miniproject.flink;

import java.util.Properties;

import org.apache.flink.core.fs.FileSystem.WriteMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import conf.ConfQuery2;
import entity.IoTSensorsDataFiltered;
import entity.Statistic;
import transformation.TransformationQuery2Optimized.IoTSensorDataClassificSessionWindow;
import transformation.TransformationQuery2Optimized.IoTSensorDataClassificWindow;
import transformation.TransformationQuery2Optimized.IoTSensorDataPlayerFilter;
import transformation.TransformationQuery2Optimized.IoTSensorDataPlayerKeySelector;
import transformation.TransformationQuery2Optimized.IoTSensorDataSensorsReducer;
import transformation.TransformationQuery2Optimized.IoTSensorDataToPlayerMapper;
import transformation.TransformationQuery2Optimized.IoTSensorDataTtartKeySelector;
import transformation.TransformationQuery2Optimized.IoTSensorDataWindowFunction;
import transformation.TransformationQuery2Optimized.IoTSensorsDataFilteredKeySelector;
import transformation.TransformationQuery2Optimized.IoTSensorsDataFilteredMapper;
import transformation.TransformationQuery2Optimized.StatisticWindowFunction;
import transformation.TransformationQuery2Optimized.StatisticWindowSessionFunction;


/**
 * Questa classe contiene la definizione della query2
 * proposta durante il corso di SABD per l'anno accademico 2016/2017 come primo quesito
 * del secondo miniprogetto di fine corso. In particolare questa versione è più ottimizzata,
 * perché oltre ad offrire i vantaggi della versione ottimizzata, consente di disporre a valle
 * della topologia la diramazione dello stream principale negli stream relativi alla finestra 
 * globale e alla finestra dei 5 minuti, evitando la trasmissione a monte di una grande mole di dati
 * In questa versione si paga in termini di precisione, perché le stime su questa finestra sono operate
 * rispetto alle stime relative alla finestra di un minuto, aggregando tali valori 
 * piuttosto che ricalcolarli nuovamente.
 * 
 * Si riporta il testo della query2:
 * 
 *	  " 2. A complemento della query precedente, si richiede di fornire la classifica 
 *		   aggiornata in tempo reale dei 5 giocatori piu veloci. 
 *		   L’output della classifica ha il seguente schema: `
 *		 			
 *		 		ts_start, ts_stop, playerid_1, avg speed1, player_id2, avg speed2,
 *		 		player_id3, avg speed3, player_id4, avg speed4, player_id5,
 *		 		avg_speed5
 *
 *		   dove
 *		   ts_start, //start of the run
 *		   ts_stop, // end of the run
 *		   player_id1, //identifier of the fastest player
 *		   avg speed1 //average speed of the run of the fastest player
 *		   player_id2, //identifier of the second fastest player
 *		   avg speed2 //average speed of the run of the second fastest player
 *		   ...
 *		 	  
 *		 Tali statistiche aggregate dovranno essere calcolate per diverse finestre temporali, in modo tale da
 *		 permettere di confrontare le prestazioni dei giocatori piu veloci durante lo svolgimento della partita. `
 *		 Le finestre temporali richieste hanno durata di:
 *		 	
 *		 	• 1 minuto;
 *		 	• 5 minuti;
 *		 	• l’intera partita."
 *		 
 */
public class Query2OptimizedV2 {

	private static final Logger logger = LoggerFactory.getLogger(Query2OptimizedV2.class);

	public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // configure event-time characteristics
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", ConfQuery2.bootstrapServersProperty);
        properties.setProperty("group.id", ConfQuery2.groupIdProperty);
        properties.setProperty("zookeeper.connect", ConfQuery2.zookeeperConnectProperty);
        properties.setProperty("auto.offset.reset", ConfQuery2.autoOffsetResetProperty);       // Always read topic from start
        DataStream<String> stream = env.addSource(new FlinkKafkaConsumer09<>(
        	ConfQuery2.nameTopic, new SimpleStringSchema(), properties) );
        
        DataStream<IoTSensorsDataFiltered> withTimestampsAndWatermarks =
        		stream
        		.map(new IoTSensorsDataFilteredMapper())
        		//.filter(new IoTSensorDataPlayerFilterOptimizedDx())
        		//.filter(new IoTSensorDataPlayerFilterOptimizedSx())
        		.filter(new IoTSensorDataPlayerFilter())
        		.assignTimestampsAndWatermarks(
        				new AscendingTimestampExtractor<IoTSensorsDataFiltered>() {
        				
					private static final long serialVersionUID = 1L;

					@Override
        	        public long extractAscendingTimestamp(IoTSensorsDataFiltered element) {
						logger.info(Long.toString(element.getTs() 
								- ConfQuery2.startMatch));
        	            return element.getTs() - ConfQuery2.startMatch;
        	        }
        	});
        
      
        DataStream<Statistic>  stream2 = withTimestampsAndWatermarks
        .keyBy(new IoTSensorsDataFilteredKeySelector())
        .timeWindow(Time.seconds(60* 1000000000L))
        .apply(new IoTSensorDataWindowFunction())        
        //faccio la join
        .map(new IoTSensorDataToPlayerMapper())
        
        //questo blocco è quello relativo al merge dei due sensori (commentare per unico sensore)
        .keyBy(new IoTSensorDataPlayerKeySelector())
        .countWindow(2)
        .reduce(new IoTSensorDataSensorsReducer());
        
         //Tempo 1 minuto
         stream2.keyBy(new IoTSensorDataTtartKeySelector())
        .countWindow(16)
        .apply(new IoTSensorDataClassificWindow())
        .writeAsText(ConfQuery2.outputOneMinute, WriteMode.OVERWRITE);
        
         //Tempo 5 minuti
         stream2.keyBy(new IoTSensorDataPlayerKeySelector())
         .countWindow(5)
        .apply(new StatisticWindowFunction())
        .keyBy(new IoTSensorDataTtartKeySelector())
        .countWindow(16)
        .apply(new IoTSensorDataClassificWindow())
        .writeAsText(ConfQuery2.outputFiveMinute, WriteMode.OVERWRITE);
         
         //Tutta la partita
         stream2.keyBy(new IoTSensorDataPlayerKeySelector())
         .window(ProcessingTimeSessionWindows.withGap(Time.seconds(300)))
         .apply( new StatisticWindowSessionFunction())      
        .countWindowAll(16)
        .apply(new IoTSensorDataClassificSessionWindow())
        .writeAsText(ConfQuery2.outputAllMatch, WriteMode.OVERWRITE);
        
        env.execute(ConfQuery2.jobName);
    }
}

