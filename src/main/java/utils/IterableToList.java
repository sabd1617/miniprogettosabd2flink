package utils;

import java.util.ArrayList;
import java.util.List;

import entity.Statistic;

public class IterableToList {
	  
	  public static <E> List<E> toList(Iterable<E> iterable) {
	    if(iterable instanceof List) {
	      return (List<E>) iterable;
	    }
	    ArrayList<E> list = new ArrayList<E>();
	    if(iterable != null) {
	      for(E e: iterable) {
	        list.add(e);
	      }
	    }
	    return list;
	  }
	  
	  public static void bubbleSort(List<Statistic> statisticList) {

	        for(int i = 0; i < statisticList.size(); i++) {
	            boolean flag = false;
	            for(int j = 0; j < statisticList.size()-1; j++) {

	                //Se l' elemento j e maggiore del successivo allora
	                //scambiamo i valori
	                if(statisticList.get(j).getAverageSpeed()>statisticList.get(j+1).getAverageSpeed()) {
	                    Statistic k = statisticList.get(j);
	                    statisticList.set(j,  statisticList.get(j+1));
	                    statisticList.set(j+1, k);
	                    flag=true; //Lo setto a true per indicare che é avvenuto uno scambio
	                }
	                

	            }

	            if(!flag) break; //Se flag=false allora vuol dire che nell' ultima iterazione
	                             //non ci sono stati scambi, quindi il metodo può terminare
	                             //poiché l' array risulta ordinato
	        }
	    }
}