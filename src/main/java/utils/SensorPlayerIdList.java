package utils;

import java.util.LinkedList;
import java.util.List;

import org.apache.flink.api.java.tuple.Tuple2;

/**
 * Classe che contiene il mapping tra il sensor id e l'id del player. 
 * Senza perdita di generalità può essere acquisito da una sorgente esterna,
 * ad esempio da un txt file. In ogni caso per ipotesi è noto a priori
 * 
 * @author falberto
 *
 */
public class SensorPlayerIdList {
	
	private static List<Tuple2<String,String>> instance = null;
	   protected SensorPlayerIdList() {
	      // Exists only to defeat instantiation.
	   }
	   public static List<Tuple2<String,String>> getInstance() {
	      if(instance == null) {
	         instance = new LinkedList<Tuple2<String,String>>();
	       //Team A:
	      	  instance.add(new Tuple2<String, String>("13","Nick Gertje"));  
	      	  instance.add(new Tuple2<String, String>("14","Nick Gertje"));
	      	  
	      	  instance.add(new Tuple2<String, String>("47","Dennis Dotterweich"));  
	      	  instance.add(new Tuple2<String, String>("16","Dennis Dotterweich"));
	      	  
	      	  instance.add(new Tuple2<String, String>("49","Niklas Waelzlein"));  
	      	  instance.add(new Tuple2<String, String>("88","Niklas Waelzlein"));
	      	  
	      	  instance.add(new Tuple2<String, String>("19","Wili Sommer"));  
	      	  instance.add(new Tuple2<String, String>("52","Wili Sommer"));
	      	  
	      	  instance.add(new Tuple2<String, String>("53","Philipp Harlass"));  
	      	  instance.add(new Tuple2<String, String>("54","Philipp Harlass"));
	      	  
	      	  instance.add(new Tuple2<String, String>("23","Roman Hartleb"));  
	      	  instance.add(new Tuple2<String, String>("24","Roman Hartleb"));
	      	  
	      	  instance.add(new Tuple2<String, String>("57","Erik Engelhardt"));  
	      	  instance.add(new Tuple2<String, String>("58","Erik Engelhardt"));
	      	  
	      	  instance.add(new Tuple2<String, String>("59","Sandro Schneider"));  
	      	  instance.add(new Tuple2<String, String>("28","Sandro Schneider"));
	      	  
	      	  //Team B:
	      	  instance.add(new Tuple2<String, String>("61","Leon Krapf"));  
	      	  instance.add(new Tuple2<String, String>("62","Leon Krapf"));
	      	  
	      	  instance.add(new Tuple2<String, String>("63","Kevin Baer"));  
	      	  instance.add(new Tuple2<String, String>("64","Kevin Baer"));
	      	  
	      	  instance.add(new Tuple2<String, String>("65","Luca Ziegler"));  
	      	  instance.add(new Tuple2<String, String>("66","Luca Ziegler"));
	      	  
	      	  instance.add(new Tuple2<String, String>("67","Ben Mueller"));  
	      	  instance.add(new Tuple2<String, String>("68","Ben Mueller"));
	      	  
	      	  instance.add(new Tuple2<String, String>("69","Vale Reitstetter"));  
	      	  instance.add(new Tuple2<String, String>("38","Vale Reitstetter"));
	      	  
	      	  instance.add(new Tuple2<String, String>("71","Christopher Lee"));  
	      	  instance.add(new Tuple2<String, String>("40","Christopher Lee"));
	      	  
	      	  instance.add(new Tuple2<String, String>("73","Leon Heinze"));  
	      	  instance.add(new Tuple2<String, String>("74","Leon Heinze"));
	      	  
	      	  instance.add(new Tuple2<String, String>("75","Leo Langhans"));  
	      	  instance.add(new Tuple2<String, String>("44","Leo Langhans"));
	      }
	      
	      return instance;
	   }
	
}
