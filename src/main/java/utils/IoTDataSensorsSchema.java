package utils;

import java.io.IOException;

import org.apache.flink.streaming.util.serialization.AbstractDeserializationSchema;

import com.fasterxml.jackson.databind.ObjectMapper;

import entity.IoTSensorsData;

/**
 * Classe che implementa una DeserializationSchema 
 * per i dati letti da Kafka
 * 
 * @author falberto
 *
 */
public class IoTDataSensorsSchema extends AbstractDeserializationSchema<IoTSensorsData> {

	private static final long serialVersionUID = 1L;
	private ObjectMapper mapper;

	@Override
	public IoTSensorsData deserialize(byte[] message) throws IOException {
		if (mapper == null) {
			mapper = new ObjectMapper();
		}
		return mapper.readValue(message, IoTSensorsData.class);
	}

	@Override
	public boolean isEndOfStream(IoTSensorsData nextElement) {
		return false;
	}


}
